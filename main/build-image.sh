#!/bin/sh
set -x
version=$(cat pom.xml | grep "<version>.*</version>$" | head -1 | awk -F'[><]' '{print $3}')
docker build -t  sample-service:$version .
docker tag sample-service:$version tandrem/test:$version
docker push tandrem/test:$version