package com.template.application;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MainApplicationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void contextLoads() {
        String result = this.restTemplate.getForObject("http://localhost:" + port + "/sample/list", String.class);
        assertEquals(result, "[{\"id\":1,\"name\":\"hello\",\"description\":\"greeting\"},{\"id\":2,\"name\":\"there\",\"description\":\"adverb\"},{\"id\":3,\"name\":\"world\",\"description\":\"subject\"}]");
    }
}