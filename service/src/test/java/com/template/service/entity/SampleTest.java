package com.template.service.entity;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SampleTest {
    Sample test;
    @BeforeEach
    void setUp() {
       test = new Sample(1,"test","only a test");
    }

    @Test
    void getId() {
        assertEquals(test.getSampleId(), 1);
    }

    @Test
    void getName() {
        assertEquals(test.getName(), "test");
    }

    @Test
    void getDescription() {
        assertEquals(test.getDescription(), "only a test");
    }

    @Test
    void setId() {
        Sample setTest = new Sample(1, "test2", "only the second test");
        setTest.setSampleId(2);
        assertEquals(setTest.getSampleId(), 2);
    }

    @Test
    void setName() {
        Sample setTest = new Sample(1, "test2", "only the second test");
        setTest.setName("new name");
        assertEquals(setTest.getName(), "new name");
    }

    @Test
    void setDescription() {
        Sample setTest = new Sample(1, "test2", "only the second test");
        setTest.setDescription("new description");
        assertEquals(setTest.getDescription(), "new description");
    }

    @Test
    void builder() {
        Sample buildTest = Sample.builder().sampleId(1).name("test").description("only a test").build();
        assertEquals(test, buildTest);
    }

    @Test
    void inequality() {
        Sample buildTest1 = Sample.builder().sampleId(2).name("test").description("only a test").build();
        assertNotEquals(test, buildTest1);
        Sample buildTest2 = Sample.builder().sampleId(1).name("test2").description("only a test").build();
        assertNotEquals(test, buildTest2);
        Sample buildTest3 = Sample.builder().sampleId(1).name("test").description("only a test3").build();
        assertNotEquals(test, buildTest3);
    }

    @Test
    void serialize() {
        ObjectMapper object = new ObjectMapper();
        try {
            String serialized = object.writeValueAsString(test);
            Sample deserialized = object.readValue(serialized, Sample.class);
            assertEquals(test, deserialized);
        } catch (JsonProcessingException e) {
            fail();
        }

    }
}