package com.template.service.mock;

import com.template.service.entity.Sample;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.function.Supplier;

@Slf4j
@SpringBootApplication
public class SampleGenerator {
    private static int sampleId = 0;
    private static final Random r = new Random();

    LinkedList<Sample> newSamples = new LinkedList<>(List.of(
        new Sample(++sampleId, "first", "first desc", LocalDateTime.now()),
        new Sample(++sampleId, "second", "second desc", LocalDateTime.now()),
        new Sample(++sampleId, "third", "third desc", LocalDateTime.now()),
        new Sample(++sampleId, "forth", "forth desc", LocalDateTime.now()),
        new Sample(++sampleId, "fifth", "fifth desc", LocalDateTime.now()),
        new Sample(++sampleId, "last", "last desc", LocalDateTime.now())
    ));

    public static void main(String[] args) {
        SpringApplication.run(SampleGenerator.class, args);
    }
    
    @Bean
    public Supplier<Message<Sample>> newSampleSupplier() {
        return () -> {
            if (newSamples.peek() != null) {
                Message<Sample> o = MessageBuilder
                        .withPayload(newSamples.peek())
                        .setHeader(KafkaHeaders.MESSAGE_KEY, Objects.requireNonNull(newSamples.poll()).getSampleId())
                        .build();
                log.info("Sample: {}", o.getPayload());
                return o;
            } else {
                return null;
            }
        };
    }
    
}
